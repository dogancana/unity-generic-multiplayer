﻿using UnityEngine;
using System.Collections;
using System;

public class BaseRPC : MonoBehaviour {

	private NetworkPrefabs networkPrefabs;

	void Start() {
		networkPrefabs = GetComponent<NetworkPrefabs> (); 
	}

	/**
	 * Prefab string must be defined in NetworkPrefabs component.
	 * 
	 */
	[RPC]
	public void SpawnObject(string prefabStr,
	                        NetworkPlayer owner,
	                        Vector3 position,
	                        Quaternion rotation) {

		Debug.Log ("Creting game object: " + prefabStr);

		try {
			GameObject prefab = findPrefab(prefabStr);
			GameObject newObject = (GameObject)MonoBehaviour.Instantiate(prefab, position, rotation);
			newObject.GetComponent<Base>().setOwner (owner);
		} catch (Exception ex) {
			Debug.LogError("Object couldn't be created.\nCause : " + ex);
		}
	}

	protected GameObject findPrefab(string prefabName) {

		GameObject prefab = null;

		// Try to find it through network prefabs component.
		if (networkPrefabs != null)
			prefab = networkPrefabs.getObjectByName (prefabName);

		// Try to find it in Resources directory
		if (prefab == null)
			prefab = Resources.Load<GameObject> (prefabName);

		if (prefab == null)
			throw new Exception ("Prefab " + prefabName + " not found in NetworkPrefabs and Resources directory.");

		return prefab;
	}
}
