﻿using UnityEngine;
using System.Collections;

public class NetworkPrefabs : MonoBehaviour {

	public GameObject[] prefabs;

	public GameObject getObjectByName(string name) {

		foreach (GameObject current in prefabs) {
			if(current.name.Equals(name)) {
				return current;
			}
		}

		return null;
	}
}