﻿using UnityEngine;
using System.Collections;

public class Base : MonoBehaviour {

	protected NetworkPlayer owner;
	protected NetworkView preDefinedNetworkView;

	protected virtual void Start() {

		Debug.Log("Base class extended.");
		if(GetComponent<NetworkView>() == null) {
			gameObject.AddComponent<NetworkView>();
		}
		else {
			preDefinedNetworkView = GetComponent<NetworkView>();
		}
	}

	protected virtual void Update() {
		// We may want to add something here in future.
	}

	protected virtual void configNetworkView() {

		/**
		 * Actually, in bathcmode there are no visual stuff, but in future we may want to remove them anyway.
		 */
		//TODO Remove visual components
	}

	public void setOwner(NetworkPlayer owner) {
		this.owner = owner;
	}
}