﻿using UnityEngine;
using System.Collections;

/**
 * Player object types will require state synchronization and they will get inputs from owner.
 * 
 */
public class Player : Base {

	private float horizontalAxis;
	private float verticalAxis;

	protected override void Start() {

		base.Start();
		Debug.Log("Player object started.");
		configNetworkView();
	}

	protected override void Update() {

		base.Update();
		if(Network.isClient) {
			Client_setMoveInput();
		}
		else {
			Server_handleMoveInput();
		}
	}

	protected override void configNetworkView() {

		base.configNetworkView();
		if(preDefinedNetworkView != null ) {
			Debug.LogWarning("Using pre-defined NetworkView configuration. This may cause performance issues." +
			                 " If you don't add any NetworkView component to current object, " +
			                 "GenericMultiplayer will add it for you with right configuration.");
			return;
		}
		((NetworkView)GetComponent<NetworkView>()).stateSynchronization = NetworkStateSynchronization.Unreliable;
	}

	private void Server_handleMoveInput() {

	}

	private void Client_setMoveInput() {
		horizontalAxis = Input.GetAxis("Horizontal");
		verticalAxis = Input.GetAxis("Vertical");
	}

	float HorizontalAxis {
		get {
			return this.horizontalAxis;
		}
	}

	float VerticalAxis {
		get {
			return this.verticalAxis;
		}
	}
}