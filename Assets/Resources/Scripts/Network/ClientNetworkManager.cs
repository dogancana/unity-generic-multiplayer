﻿using UnityEngine;
using System.Collections;

/**
 * ClientNetworkManager should be responsible for connecting to a game and recieve
 * current level information.
 * 
 */
public class ClientNetworkManager : MonoBehaviour {

	private HostData[] hostList;

	void Start() {
		MasterServer.ipAddress 		= NetworkConfig.MASTER_SERVER_ADDR;
		MasterServer.port 			= NetworkConfig.MASTER_SERVER_PORT;
		Network.natFacilitatorIP 	= NetworkConfig.MASTER_SERVER_ADDR;
		Network.natFacilitatorPort 	= NetworkConfig.NAT_FACILITATOR_PORT;
	}

	private void RefreshHostList() {
		MasterServer.RequestHostList(NetworkConfig.TYPE_NAME);
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent) {
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}

	private void JoinServer(HostData hostData) {
		Network.Connect(hostData);
	}
	
	void OnConnectedToServer() {
		Debug.Log("Connected to server");
	}

	void OnGUI() {
		if (!Network.isClient && !Network.isServer) {
			if (GUI.Button(new Rect(100, 250, 250, 100), "Refresh Hosts"))
				RefreshHostList();
			
			if (hostList != null) {
				for (int i = 0; i < hostList.Length; i++) {
					if (GUI.Button(new Rect(400, 100 + (110 * i), 300, 100), hostList[i].gameName))
						JoinServer(hostList[i]);
				}
			}
		}
	}
}
