﻿using UnityEngine;
using System.Collections;

/**
 * ServerNetworkMaster should be responsible for creating a server and hold 
 * connected player details.
 * 
 */
public class ServerNetworkMaster : MonoBehaviour {

	public string playerPrefabString = "PlayerPrefab";
	public int expectedPlayers = 1;

	private int playerCount = 0;
	private NetworkView networkView;

	void Start() {
		// Setup master server configuration
		MasterServer.ipAddress 		= NetworkConfig.MASTER_SERVER_ADDR;
		MasterServer.port 			= NetworkConfig.MASTER_SERVER_PORT;
		Network.natFacilitatorIP 	= NetworkConfig.MASTER_SERVER_ADDR;
		Network.natFacilitatorPort 	= NetworkConfig.NAT_FACILITATOR_PORT;

		// Get references
		networkView = GetComponent<NetworkView> ();

		StartServer();
	}
	
	private void StartServer() {
		Network.InitializeServer(4, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(NetworkConfig.TYPE_NAME, NetworkConfig.GAME_NAME);
	}

	public void StartGame() {
		Debug.Log("Game is starting");
		spawnOtherObjects ();
//		spawnPlayers ();
	}

	private void spawnOtherObjects() {
		// Set owner of these objects as server
		GetComponent<NetworkView>().RPC("SpawnObject", RPCMode.All, "Environment", Network.player, Vector3.zero, Quaternion.identity );
	}

	private void spawnPlayers() {
		foreach(NetworkPlayer player in Network.connections) {
			Debug.Log("RPC call for creating player");
			networkView.RPC("SpawnObject", RPCMode.All, playerPrefabString, player, Vector3.zero, Quaternion.identity );
		}
	}

	void OnServerInitialized() {
		Debug.Log("Server Initializied");
	}
	
	void OnPlayerConnected(NetworkPlayer player) {
		Debug.Log("Player connected from " + player.ipAddress + ":" + player.port);
		if(++playerCount == expectedPlayers) {
			StartGame();
		}
	}
	
	void OnPlayerDisconnected(NetworkPlayer player) {
		Debug.Log("Player disconnected: " + player);
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}

	/**
	 * This will be a batch application
	 * 
	void OnGUI() {
		if (!Network.isClient && !Network.isServer) {
			if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server")) {
				StartServer();
			}
		}
	}
	*/
}
