﻿using UnityEngine;
using System.Collections;

public class NetworkConfig {

	public static string MASTER_SERVER_ADDR = "127.0.0.1";
	public static int MASTER_SERVER_PORT 	= 23466;
	public static int NAT_FACILITATOR_PORT 	= 50005;
	public static string TYPE_NAME 		 	= "com.dogancan.chase";
	public static string GAME_NAME 		 	= "dev";
}